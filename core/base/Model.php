<?php

namespace core\base;

use PDO;

/**
 *
 */
abstract class Model
{
    /**
     * @return mixed
     */
    protected static function getDB()
    {
        static $db = null;

        if ($db === null) {
            $config = require(APP_PATH . '/config/db.php');
            $db = new PDO($config['dsn'], $config['username'], $config['password']);
        }

        return $db;
    }
}
