<?php

namespace core\base;

use core\helpers\Inflector;

/**
 *
 */
abstract class Controller
{
    /**
     * @return void
     */
    public function __call($name, $args)
    {
        $method = Inflector::getActionName($name);

        if (method_exists($this, $method)) {
            $output = call_user_func_array([$this, $method], $args);
            print($output);
        } else {
            throw new \Exception("Method $method not found in controller " . get_class($this));
        }
    }

    /**
     * @return string
     */
    public function render($view, $params = [])
    {
        $class = Inflector::getClassName(get_class($this));

        $controller = substr($class, 0, strrpos($class, 'Controller'));
        $controller = Inflector::camel2id($controller);

        return View::renderTemplate($controller . '/' . $view, $params);
    }

    /**
     * @return void
     */
    public function goHome()
    {
        $this->redirect('/');
    }

    /**
     * @return void
     */
    public function redirect($url)
    {
        header("Location: $url");
        exit();
    }
}
