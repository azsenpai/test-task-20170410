<?php

namespace core\base;

/**
 *
 */
class Error
{
    /**
     * @var string
     */
    public static $route = 'site/error';

    /**
     * @return void
     */
    public static function errorHandler($level, $message, $file, $line)
    {
        if (error_reporting() !== 0) {
            throw new \ErrorException($message, 0, $level, $file, $line);
        }
    }

    /**
     * @return void
     */
    public static function exceptionHandler($exception)
    {
        $code = $exception->getCode();

        http_response_code($code);

        $output = View::renderTemplate(static::$route, ['exception' => $exception], true);
        print($output);
    }
}
