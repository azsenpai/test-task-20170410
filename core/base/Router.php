<?php

namespace core\base;

use core\helpers\ArrayHelper;
use core\helpers\Inflector;
use core\helpers\Url;

/**
 *
 */
class Router
{
    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @var array
     */
    protected $params = [];

    /**
     * @var string
     */
    protected $defaultController = 'site';

    /**
     * @var string
     */
    protected $defaultAction = 'index';

    /**
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @return void
     */
    public function add($url, $params)
    {
        $url = Url::removeQueryString($url);
        $url = Url::normalize($url);

        $this->routes[$url] = $params;
    }

    /**
     * @return void
     */
    public function batchAdd($items)
    {
        foreach ($items as $item) {
            $url = ArrayHelper::getValue($item, 'url');
            $params = ArrayHelper::getValue($item, 'params', []);

            $this->add($url, $params);
        }
    }

    /**
     * @return void
     */
    public function dispatch($url)
    {
        $url = Url::removeQueryString($url);
        $url = Url::normalize($url);

        if (isset($this->routes[$url])) {
            $this->params = $this->routes[$url];
        } elseif (!empty($url)) {
            $parts = explode('/', $url);

            $this->params = [];

            if (count($parts) == 1) {
                $this->params['controller'] = $parts[0];
            } else if (count($parts) > 1) {
                $this->params['controller'] = $parts[0];
                $this->params['action'] = $parts[1];
            }
        }

        $controller = ArrayHelper::getValue($this->params, 'controller', $this->defaultController);
        $controller = Inflector::getControllerClass($controller);

        if (class_exists($controller)) {
            $controller_object = new $controller();

            $action = ArrayHelper::getValue($this->params, 'action', $this->defaultAction);

            if (is_callable([$controller_object, $action])) {
                $controller_object->$action();
            } else {
                throw new \Exception("Method $action (in controller $controller) not found");
            }
        } else {
            throw new \Exception("Controller class $controller not found");
        }
    }
}
