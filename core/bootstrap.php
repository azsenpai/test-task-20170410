<?php

require dirname(__DIR__) . '/vendor/autoload.php';

defined('CORE_PATH') or define('CORE_PATH', __DIR__);

defined('APP_PATH') or define('APP_PATH', dirname(CORE_PATH) . '/app');
defined('WEB_PATH') or define('WEB_PATH', dirname(CORE_PATH) . '/web');

error_reporting(E_ALL);

set_error_handler('core\base\Error::errorHandler');
set_exception_handler('core\base\Error::exceptionHandler');

require APP_PATH . '/bootstrap.php';
