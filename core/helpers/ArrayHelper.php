<?php

namespace core\helpers;

/**
 *
 */
class ArrayHelper
{
    /**
     * @return mixed
     */
    public static function getValue($items, $key, $default = null)
    {
        if (isset($items[$key])) {
            return $items[$key];
        }

        return $default;
    }
}
