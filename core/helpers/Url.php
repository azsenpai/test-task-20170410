<?php

namespace core\helpers;

/**
 *
 */
class Url
{
    /**
     * @return string
     */
    public static function normalize($url)
    {
        $url = trim($url, '/');

        return $url;
    }

    /**
     * @return string
     */
    public static function removeQueryString($url)
    {
        $pos = strpos($url, '?');

        if ($pos !== false) {
            $url = substr($url, 0, $pos);
        }

        return $url;
    }
}
