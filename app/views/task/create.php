<?php

use core\base\View;

View::$breadcrumbs[] = ['title' => 'Создать задачу'];

?>
<div class="task-create">
    <form class="form" method="post" enctype="multipart/form-data">
        <div class="form-group <?= isset($model->errors['name']) ? 'has-error' : '' ?>">
            <label for="task-name">Имя</label>
            <input class="form-control" name="Task[name]" id="task-name" placeholder="Ваше имя" value="<?= $model->name ?>">
        </div>

        <div class="form-group <?= isset($model->errors['email']) ? 'has-error' : '' ?>">
            <label for="task-email">E-mail</label>
            <input class="form-control" name="Task[email]" id="task-email" placeholder="Ваш e-mail" value="<?= $model->email ?>">
        </div>

        <div class="form-group <?= isset($model->errors['description']) ? 'has-error' : '' ?>">
            <label for="task-description">Задача</label>
            <textarea class="form-control" name="Task[description]" id="task-description" placeholder="Описание задачи"><?= $model->description ?></textarea>
        </div>

        <div class="form-group">
            <label for="task-images">Картинки</label>
            <input name="Task[images][]" id="task-images" type="file" multiple accept="image/jpeg,image/gif,image/png">
        </div>

        <div class="text-center">
            <span class="btn btn-sm btn-primary btn-preview">Предварительный просмотр</span>
            <button class="btn btn-success">Сохранить</button>
        </div>
    </form>

    <div class="preview">
        <p class="text-right">
            <span class="btn btn-sm btn-danger btn-preview-close glyphicon glyphicon-remove"></span>
        </p>

        <ul class="list-group">
            <li class="list-group-item"><b>Ваше имя:</b> <span class="task-name-value"></span></li>
            <li class="list-group-item"><b>Ваш e-mail:</b> <span class="task-email-value"></span></li>
            <li class="list-group-item"><b>Описание задачи:</b> <span class="task-description-value"></span></li>
        </ul>

        <div class="preview-images"></div>
    </div>
</div>