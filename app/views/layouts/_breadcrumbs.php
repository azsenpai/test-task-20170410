<?php

use core\base\View;

?>
<?php if (count(View::$breadcrumbs) > 0): ?>
    <ol class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <?php foreach (View::$breadcrumbs as $breadcrumb): ?>
            <?php if (empty($breadcrumb['href'])): ?>
                <li><?= $breadcrumb['title'] ?></li>
            <?php else: ?>
                <li><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['title'] ?></a></li>
            <?php endif ?>
        <?php endforeach ?>
    </ol>
<?php endif ?>