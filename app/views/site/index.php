<p>
    <a href="/task/create" class="btn btn-success">Создать задачу</a>
</p>

<hr>

<table class="table">
    <thead>
        <tr>
            <th>Имя</th>
            <th>E-mail</th>
            <th>Описание</th>
            <th>Картинки</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($tasks as $task): ?>
        <tr>
            <td><?= $task['name'] ?></td>
            <td><?= $task['email'] ?></td>
            <td><?= $task['description'] ?></td>
            <td>
                <?php foreach ($task['images'] as $i => $image): ?>
                    <a class="btn btn-xs btn-info" href="/uploads/<?= $image['filename'] ?>"><?= $i+1 ?></a>
                <?php endforeach ?>
            </td>
        </tr>
        <?php endforeach ?>
    </tbody>
</table>