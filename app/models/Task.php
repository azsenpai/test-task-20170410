<?php

namespace app\models;

use PDO;
use core\base\Model;
use core\helpers\ArrayHelper;

/**
 *
 */
class Task extends Model
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $description;

    /**
     * @var integer
     */
    public $completed;

    /**
     * @var array
     */
    public $errors = [];

    /**
     * @return array
     */
    public static function getAll()
    {
        $dbh = static::getDb();

        $sql = 'SELECT * FROM `task`';

        $sth = $dbh->prepare($sql);
        $sth->execute();

        $tasks = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach ($tasks as $i => $task) {
            $tasks[$i]['images'] = static::getImages($task['id']);
        }

        return $tasks;
    }

    /**
     * @return array
     */
    public static function getImages($task_id)
    {
        $dbh = static::getDb();

        $sql = 'SELECT * FROM `image` WHERE `task_id` = :task_id';

        $sth = $dbh->prepare($sql);
        $sth->execute([':task_id' => $task_id]);

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return boolean
     */
    public function load($data)
    {
        $this->name = ArrayHelper::getValue($data, 'name');
        $this->email = ArrayHelper::getValue($data, 'email');
        $this->description = ArrayHelper::getValue($data, 'description');

        if (empty($this->name)) {
            $this->errors['name'] = true;
        }

        if (empty($this->email) || !filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->errors['email'] = true;
        }

        if (empty($this->description)) {
            $this->errors['description'] = true;
        }

        return count($this->errors) == 0;
    }

    /**
     * @return boolean
     */
    public function save()
    {
        $dbh = static::getDb();

        $sql = 'INSERT INTO task(`name`, `email`, `description`)
            VALUES(:name, :email, :description)';

        $sth = $dbh->prepare($sql);

        $sth->execute([
            ':name' => $this->name,
            ':email' => $this->email,
            ':description' => $this->description,
        ]);

        $task_id = $dbh->lastInsertId();

        if (empty($task_id)) {
            return false;
        }

        $this->uploadImages($task_id);

        return true;
    }

    /**
     * @return void
     */
    public function uploadImages($task_id)
    {
        if (!isset($_FILES['Task']) || !isset($_FILES['Task']['type']['images'])) {
            return;
        }

        $dbh = static::getDb();

        $sql = 'INSERT INTO image(`task_id`, `name`, `filename`, `size`)
            VALUES(:task_id, :name, :filename, :size)';

        $sth = $dbh->prepare($sql);

        $accept = [
            'image/jpeg' => 'jpeg',
            'image/gif' => 'gif',
            'image/png' => 'png',
        ];

        foreach ($_FILES['Task']['type']['images'] as $i => $type) {
            if (!isset($accept[$type]) || $_FILES['Task']['error']['images'][$i] != UPLOAD_ERR_OK) {
                continue;
            }

            $name = $_FILES['Task']['name']['images'][$i];
            $filename = uniqid() . '.' . $accept[$type];
            $size = $_FILES['Task']['size']['images'][$i];

            if (move_uploaded_file($_FILES['Task']['tmp_name']['images'][$i], UPLOADS . '/' . $filename)) {
                $sth->execute([
                    ':task_id' => $task_id,
                    ':name' => $name,
                    ':filename' => $filename,
                    ':size' => $size,
                ]);

                Image::resize(UPLOADS . '/' . $filename, $accept[$type]);
            }
        }
    }
}
