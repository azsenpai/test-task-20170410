<?php

namespace app\models;

use core\base\Model;

/**
 *
 */
class Migration extends Model
{
    /**
     * @return void
     */
    public function up()
    {
        $this->createUserTable();
        $this->createImageTable();
    }

    /**
     * @return void
     */
    public function createUserTable()
    {
        $dbh = static::getDB();

        $sql = 'CREATE TABLE IF NOT EXISTS task(
            id INT NOT NULL AUTO_INCREMENT,
            PRIMARY KEY(id),
            name VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL,
            description TEXT NOT NULL,
            completed INT(1) NOT NULL DEFAULT 0,
            created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
        )';

        $dbh->prepare($sql)->execute();
    }

    /**
     * @return void
     */
    public function createImageTable()
    {
        $dbh = static::getDB();

        $sql = 'CREATE TABLE IF NOT EXISTS image(
            id INT NOT NULL AUTO_INCREMENT,
            PRIMARY KEY(id),
            task_id INT NOT NULL,
            FOREIGN KEY (task_id) REFERENCES task(id),
            name VARCHAR(255) NOT NULL,
            filename VARCHAR(255) NOT NULL,
            size INT NOT NULL,
            created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
        )';

        $dbh->prepare($sql)->execute();

        $sql = 'CREATE INDEX `idx-image-task_id`
            ON image(task_id)';

        $dbh->prepare($sql)->execute();
    }
}
