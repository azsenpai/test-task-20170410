<?php

namespace app\models;

use core\base\Model;

/**
 *
 */
class Image extends Model
{
    /**
     * @var integer
     */
    const MAX_WIDTH = 320;

    /**
     * @var integer
     */
    const MAX_HEIGHT = 240;

    /**
     * @return void
     */
    public static function resize($filename, $type)
    {
        $resize = false;

        list($src_w, $src_h) = getimagesize($filename);

        $dst_w = $src_w;
        $dst_h = $src_h;

        if ($dst_w > static::MAX_WIDTH) {
            $dst_w = static::MAX_WIDTH;
            $dst_h *= $dst_w / $src_w;
            $resize = true;
        } else if ($dst_h > static::MAX_HEIGHT) {
            $dst_h = static::MAX_HEIGHT;
            $dst_w *= $dst_h / $src_h;
            $resize = true;
        }

        if ($resize) {
            $dst_image = imagecreatetruecolor($dst_w, $dst_h);
            $src_image = static::getImageResource($filename, $type);

            if (imagecopyresized($dst_image, $src_image, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h)) {
                static::saveImage($type, $dst_image, $filename);
            }
        }
    }

    /**
     * @return resource
     */
    public static function getImageResource($filename, $type)
    {
        switch ($type) {
            case 'jpeg':
                return imagecreatefromjpeg($filename);

            case 'gif':
                return imagecreatefromgif($filename);

            case 'png':
                return imagecreatefrompng($filename);
        }

        return null;
    }

    /**
     * @return resource
     */
    public static function saveImage($type, $image, $to)
    {
        switch ($type) {
            case 'jpeg':
                imagejpeg($image, $to);
                break;

            case 'gif':
                imagegif($image, $to);
                break;

            case 'png':
                imagepng($image, $to);
                break;
        }
    }
}
