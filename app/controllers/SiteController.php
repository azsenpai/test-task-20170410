<?php

namespace app\controllers;

use core\base\Controller;
use app\models\Task;

/**
 *
 */
class SiteController extends Controller
{
    /**
     *
     */
    public function actionIndex()
    {
        $tasks = Task::getAll();

        return $this->render('index', [
            'tasks' => $tasks,
        ]);
    }
}
