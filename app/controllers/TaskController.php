<?php

namespace app\controllers;

use core\base\Controller;
use app\models\Task;

/**
 *
 */
class TaskController extends Controller
{
    /**
     *
     */
    public function actionCreate()
    {
        $model = new Task();

        if (isset($_POST['Task'])) {
            if ($model->load($_POST['Task']) && $model->save()) {
                return $this->goHome();
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
}
