jQuery(function($) {
    if ($('.task-create').length == 0) {
        return;
    }

    var $form = $('.form');

    var $name = $form.find('[name="Task[name]"]');
    var $email = $form.find('[name="Task[email]"]');
    var $description = $form.find('[name="Task[description]"]');

    var reg_email = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,6}$/i;

    var $preview = $('.preview');
    var $preview_images = $('.preview-images');

    $('.btn-preview').on('click', function() {
        previewValues();

        $form.hide();
        $preview.show();
    });

    $('.btn-preview-close').on('click', function() {
        $form.show();
        $preview.hide();
    });

    $form.on('submit', function(e) {
        if (!validate()) {
            e.preventDefault();
        }
    });

    $('[name="Task[images][]"]').on('change', function(e) {
        var files = e.target.files;

        $preview_images.html('');

        for (var i = 0; i < files.length; i ++) {
            var reader = new FileReader();

            reader.onload = function() {
                var $img = $('<img>');

                $img.attr('src', this.result);
                $img.addClass('preview-img');

                $preview_images.append($img);
            };

            reader.readAsDataURL(files[i]);
        }
    });

    function previewValues() {
        $('.task-name-value').text($name.val());
        $('.task-email-value').text($email.val());
        $('.task-description-value').text($description.val());
    }

    function validate() {
        removeErrors();

        var result = true;

        if (!$.trim($name.val()).length) {
            result = false;
            addError($name);
        }

        if ($email.val().search(reg_email) == -1) {
            result = false;
            addError($email);
        }

        if (!$.trim($description.val()).length) {
            result = false;
            addError($description);
        }

        return result;
    }

    function removeErrors() {
        removeError($name);
        removeError($email);
        removeError($description);
    }

    function addError($field) {
        $field.parents('.form-group').addClass('has-error');
    }

    function removeError($field) {
        $field.parents('.form-group').removeClass('has-error');
    }
});