<?php

require dirname(__DIR__) . '/core/bootstrap.php';

$router = new core\base\Router();

$router->dispatch($_SERVER['REQUEST_URI']);
